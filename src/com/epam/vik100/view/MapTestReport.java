package com.epam.vik100.view;

import java.util.ArrayList;
import java.util.List;

import com.epam.vik100.model.MapTester;
import com.epam.vik100.model.TestReport;

public class MapTestReport<T> implements TestReport {
	private List<MapTester<T>> tests;
	private int amount;

	public List<MapTester<T>> getTests() {
		return tests;
	}

	public void setTests(List<MapTester<T>> tests) {
		this.tests = tests;
	}

	public MapTestReport(int amount) {
		tests = new ArrayList<>();
		this.amount = amount;
	}

	public void add(MapTester<T> test) {
		tests.add(test);
	}

	private void printLine() {
		int i = 85;
		System.out.print("\t");
		while (i >= 0) {
			System.out.print("-");
			i--;
		}
		System.out.println();
	}

	public void showReport() {
		System.out
				.println("\n\n\t\t*   *   *   THE RESULTS  maplike collections - " + amount + " elements *   *   *\n");
		printLine();
		System.out.printf("\t|%-20s|%15s|%15s|%15s|%15s|", "Collection's name", "Put", "Get", "Remove",
				"ContainsValue");
		System.out.println();
		printLine();
		for (MapTester<T> test : tests) {
			System.out.println(test);
		}
		printLine();
	}
}
