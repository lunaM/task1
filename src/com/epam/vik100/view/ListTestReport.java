package com.epam.vik100.view;

import java.util.ArrayList;
import java.util.List;

import com.epam.vik100.model.ListTester;
import com.epam.vik100.model.TestReport;

public class ListTestReport<T> implements TestReport {
	private List<ListTester<T>> tests;
	private int amount;

	public List<ListTester<T>> getTests() {
		return tests;
	}

	public void setTests(List<ListTester<T>> tests) {
		this.tests = tests;
	}

	public ListTestReport(int amount) {
		tests = new ArrayList<>();
		this.amount = amount;
	}

	public void add(ListTester<T> test) {
		tests.add(test);
	}

	private void printLine() {
		int i = 117;
		System.out.print("\t");
		while (i >= 0) {
			System.out.print("-");
			i--;
		}
		System.out.println();
	}

	public void showReport() {
		System.out.println("\n\n\t\t*   *   *   THE RESULTS  listlike collections - " + amount+ " elements *   *   *\n");
		printLine();
		System.out.printf("\t|%-20s|%-15s|%-15s|%-15s|%-15s|%-15s|%-15s|", "Collection's name", "AddBegin", "AddCenter",
				"AddEnd", "RemoveBegin", "RemoveCenter", "RemoveEnd");
		System.out.println();
		printLine();
		for (ListTester<T> test : tests) {
			System.out.println(test);
		}
		printLine();
	}
}
