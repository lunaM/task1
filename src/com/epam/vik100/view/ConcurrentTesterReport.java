package com.epam.vik100.view;

import java.util.ArrayList;
import java.util.List;

import com.epam.vik100.model.ConcurrentTester;
import com.epam.vik100.model.TestReport;

public class ConcurrentTesterReport<T> implements TestReport {
	private List<ConcurrentTester<T>> tests;
	private int amount;

	public List<ConcurrentTester<T>> getTests() {
		return tests;
	}

	public void setTests(List<ConcurrentTester<T>> tests) {
		this.tests = tests;
	}

	public ConcurrentTesterReport(int amount) {
		tests = new ArrayList<>();
		this.amount = amount;
	}

	public void add(ConcurrentTester<T> test) {
		tests.add(test);
	}

	private void printLine() {
		int i = 79;
		System.out.print("\t");
		while (i >= 0) {
			System.out.print("-");
			i--;
		}
		System.out.println();
	}

	public void showReport() {
		System.out.println("\n\n\t\t*   *   *   THE RESULTS  in concurrent mode - " + amount + " elements *   *   *\n");
		printLine();
		System.out.printf("\t|%-30s|%-15s|%-15s|%-15s|", "Collection's name", "Add", "Find", "Remove");
		System.out.println();
		printLine();
		for (ConcurrentTester<T> test : tests) {
			System.out.println(test);
		}
		printLine();
	}
}
