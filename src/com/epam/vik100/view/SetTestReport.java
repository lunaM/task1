package com.epam.vik100.view;

import java.util.ArrayList;
import java.util.List;

import com.epam.vik100.model.TestReport;
import com.epam.vik100.model.TestableSet;

public class SetTestReport<T> implements TestReport {
	private List<TestableSet<T>> tests;
	private int amount;

	public List<TestableSet<T>> getTests() {
		return tests;
	}

	public void setTests(List<TestableSet<T>> tests) {
		this.tests = tests;
	}

	public SetTestReport(int amount) {
		tests = new ArrayList<>();
		this.amount = amount;
	}

	public void add(TestableSet<T> test) {
		tests.add(test);
	}

	private void printLine() {
		int i = 64;
		System.out.print("\t");
		while (i >= 0) {
			System.out.print("-");
			i--;
		}
		System.out.println();
	}

	public void showReport() {
		System.out
				.println("\n\n\t\t*   *   *   THE RESULTS  setlike collections - " + amount + " elements *   *   *\n");
		printLine();
		System.out.printf("\t|%-30s|%10s|%10s|%10s|", "Collection's name", "Add", "Remove", "Contains");
		System.out.println();
		printLine();
		for (TestableSet<T> test : tests) {
			System.out.println(test);
		}
		printLine();
	}
}
