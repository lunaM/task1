package com.epam.vik100.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

import com.epam.vik100.model.ConcurrentTester;
import com.epam.vik100.model.Generator;
import com.epam.vik100.model.ListTester;
import com.epam.vik100.model.MapTester;
import com.epam.vik100.model.MultisetTester;
import com.epam.vik100.model.SetTester;
import com.epam.vik100.model.TestReport;
import com.epam.vik100.model.TestableSet;
import com.epam.vik100.view.ConcurrentTesterReport;
import com.epam.vik100.view.ListTestReport;
import com.epam.vik100.view.MapTestReport;
import com.epam.vik100.view.SetTestReport;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.LinkedHashMultiset;

public class TestingController {
	public static void main(String[] args) {
		int[][] testProperties = { { 10, 10 }, { 100, 10 }, { 1000, 10 }, { 10000, 10 }, { 100000, 10 } };
		for (int i = 0; i < testProperties.length; i++) {
			TestingController.test(testProperties[i][0], testProperties[i][1]);
		}
	}

	public static void test(int amountOfElements, int cycles) {
		// testing speed of Collections that implemets List
		// ArrayList LinkedList Vector
		List<TestReport> reports = new ArrayList<>();
		ListTestReport<Integer> listsToTest = new ListTestReport<>(amountOfElements);
		List<Integer> arrayListTest = new ArrayList<>(amountOfElements);
		listsToTest.add(new ListTester<Integer>(arrayListTest, cycles, amountOfElements));
		List<Integer> linkedListTest = new LinkedList<>(arrayListTest);
		listsToTest.add(new ListTester<>(linkedListTest, cycles, amountOfElements));
		Vector<Integer> vectorTest = new Vector<>(arrayListTest);
		listsToTest.add(new ListTester<>(vectorTest, cycles, amountOfElements));
		for (ListTester<Integer> list : listsToTest.getTests()) {
			list.fillList(0);
			list.findAddingTimeBegin(5);
			list.fillList(0);
			list.findAddingTimeCenter(5);
			list.fillList(0);
			list.findAddingTimeEnd(5);
			list.fillList(0);
			list.findRemovingTimeBegin(5);
			list.fillList(0);
			list.findRemovingTimeCenter(5);
			list.fillList(0);
			list.findRemovingTimeEnd(5);
			list.fillList(0);
			list.findAddingTimeCenter(5);
		}
		reports.add(listsToTest);

		// testing speed of Collections that implemets Set
		// HashSet LinkedHashSet TreeSet
		SetTestReport<Integer> setsToTest = new SetTestReport<>(amountOfElements);
		setsToTest.add(new SetTester<>(new HashSet<>(arrayListTest), cycles));
		setsToTest.add(new SetTester<>(new LinkedHashSet<>(arrayListTest), cycles));
		setsToTest.add(new SetTester<>(new TreeSet<>(arrayListTest), cycles));
		setsToTest.add(new MultisetTester<Integer>(HashMultiset.create(arrayListTest), cycles));
		setsToTest.add(new MultisetTester<Integer>(LinkedHashMultiset.create(arrayListTest), cycles));
		for (TestableSet<Integer> set : setsToTest.getTests()) {
			set.fillWithElements(0, amountOfElements, new Generator<Integer>() {
				Random random = new Random(47);

				@Override
				public Integer next() {
					return random.nextInt();
				}
			});
			set.findAddingTime(6);
			set.findFindingElementTime(6);
			set.findRemovingTime(6);
		}
		reports.add(setsToTest);

		// testing speed of Collections that implemets Map
		// HashMap LinkedHashMap TreeMap
		MapTestReport<String> mapsToTest = new MapTestReport<>(amountOfElements);
		mapsToTest.add(new MapTester<String>(new HashMap<Integer, String>(), cycles));
		mapsToTest.add(new MapTester<String>(new LinkedHashMap<Integer, String>(), cycles));
		mapsToTest.add(new MapTester<String>(new TreeMap<Integer, String>(), cycles));
		for (MapTester<String> map : mapsToTest.getTests()) {
			map.fillMap("test", amountOfElements);
			map.findPuttingTime("test");
			map.findFindingByValueTime("test2");
			map.findGettingTime();
			map.findRemovingTime();
		}
		reports.add(mapsToTest);

		// testing collections in concurrent mode
		// synchronizedList CopyOnWriteArrayList CopyOnWriteArraySet
		// synchronizedSet ConcurrentSkipListSet
		List<String> listToTest = new ArrayList<>(1000);
		Collections.fill(listToTest, "test");
		ConcurrentTesterReport<String> concurrentTester = new ConcurrentTesterReport<>(amountOfElements);
		concurrentTester.add(new ConcurrentTester<String>(Collections.synchronizedList(listToTest), cycles));
		concurrentTester.add(new ConcurrentTester<String>(new CopyOnWriteArrayList<>(listToTest), cycles));
		concurrentTester.add(new ConcurrentTester<String>(new CopyOnWriteArraySet<>(listToTest), cycles));
		concurrentTester
				.add(new ConcurrentTester<String>(Collections.synchronizedSet(new HashSet(listToTest)), cycles));
		concurrentTester.add(new ConcurrentTester<String>(new ConcurrentSkipListSet<>(listToTest), cycles));
		for (ConcurrentTester<String> tester : concurrentTester.getTests()) {
			tester.findAddingTime("Test 2");
			tester.findFindingTime("Test 2");
			tester.findRemovingTime("Test 2");
		}
		reports.add(concurrentTester);
		for (TestReport report : reports) {
			report.showReport();
		}
	}
}
