package com.epam.vik100.model;

public interface Generator<T> {
	T next();
}
