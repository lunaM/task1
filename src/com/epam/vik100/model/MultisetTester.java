package com.epam.vik100.model;

import java.util.Formatter;

import com.google.common.collect.Multiset;

public class MultisetTester<T> implements TestableSet<T> {
	private Multiset<T> set;
	private String collectionName;
	private long addingTime;
	private long removingTime;
	private long findElementTime;
	private int cycles;

	public MultisetTester(Multiset<T> set, int cycles) {
		this.cycles = cycles;
		this.set = set;
		collectionName = set.getClass().getSimpleName() + " (GUAVA)";
	}

	public Multiset<T> getList() {
		return set;
	}

	public long findAddingTime(T element) {
		long start = System.nanoTime();
		int iter = cycles;
		while (iter > 0) {
			set.add(element);
			iter--;
		}
		addingTime = (System.nanoTime() - start) / cycles;
		return addingTime;
	}

	public long findRemovingTime(T element) throws TooFewElementsException {
		if (set.size() < cycles) {
			throw new TooFewElementsException();
		}
		long start = System.nanoTime();
		int iter = cycles;
		while (iter > 0) {
			set.remove(element);
			iter--;
		}
		removingTime = (System.nanoTime() - start) / cycles;
		return removingTime;
	}

	public long findFindingElementTime(T element) {
		set.add(element);
		int iter = cycles;
		long start = System.nanoTime();
		while (iter > 0) {
			set.contains(element);
			iter--;
		}
		findElementTime = System.nanoTime() - start;
		return findElementTime;
	}

	private String convinientTime(long time) {
		return time + " ns";
	}

	public String toString() {
		Formatter formatter = new Formatter();
		formatter.format("\t|%-30s|%10s|%10s|%10s|", collectionName, convinientTime(addingTime),
				convinientTime(removingTime), convinientTime(findElementTime));
		String result = formatter.toString();
		formatter.close();
		return result;
	}

	public String getCollectionName() {
		return collectionName;
	}

	@Override
	public void fillWithElements(T element, int amount, Generator<T> generator) {
		while (amount > 0) {
			set.add(generator.next());
			amount--;
		}
	}
}
