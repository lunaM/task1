package com.epam.vik100.model;

public interface TestableSet<T> {
	long findAddingTime(T element);
	long findFindingElementTime(T element);
	long findRemovingTime(T element);
	void fillWithElements(T element, int amount, Generator<T> generator);
}
