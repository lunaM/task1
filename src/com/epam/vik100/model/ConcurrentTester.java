package com.epam.vik100.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Formatter;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ConcurrentTester<T> {
	private Collection<T> collection;
	private int cycles;
	private String collectionName;
	private volatile long addingTime;
	private volatile long findingTime;
	private volatile long removingTime;

	public int getCycles() {
		return cycles;
	}

	public void setCycles(int cycles) {
		this.cycles = cycles;
	}

	public ConcurrentTester(Collection<T> collection, int cycles) {
		this.collection = collection;
		this.cycles = cycles;
		collectionName = collection.getClass().getSimpleName();
	}

	class Adder implements Callable<Long> {
		private T dataToAdd;
		private final CyclicBarrier barrier;

		public Adder(T dataToAdd, CyclicBarrier barrier) {
			this.dataToAdd = dataToAdd;
			this.barrier = barrier;
		}

		@Override
		public Long call() throws Exception {
			long start = System.nanoTime();
			collection.add(dataToAdd);
			long end = System.nanoTime() - start;
			barrier.await();
			return end;
		}
	}

	class Finder implements Callable<Long> {
		private T dataToFind;

		public Finder(T dataToFind) {
			this.dataToFind = dataToFind;
		}

		@Override
		public Long call() throws Exception {
			long start = System.nanoTime();
			collection.add(dataToFind);
			return System.nanoTime() - start;
		}
	}

	class Remover implements Callable<Long> {
		private T dataToDelete;

		public Remover(T dataToDelete) {
			this.dataToDelete = dataToDelete;
		}

		@Override
		public Long call() throws Exception {
			long start = System.nanoTime();
			collection.remove(dataToDelete);
			return System.nanoTime() - start;
		}
	}

	public long findAddingTime(T dataToAdd) {
		ExecutorService addExecutor = Executors.newFixedThreadPool(cycles);
		List<Future<Long>> result = new ArrayList<>();
		long time = 0;
		for (int i = 0; i < cycles; i++) {
			result.add(addExecutor.submit(new Finder(dataToAdd)));
		}
		for (Future<Long> future : result) {
			try {
				time += future.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		addExecutor.shutdown();
		addingTime = time / cycles;
		return addingTime;
	}

	public long findFindingTime(T dataToFind) {
		ExecutorService findExecutor = Executors.newFixedThreadPool(cycles);
		List<Future<Long>> result = new ArrayList<>();
		long time = 0;
		for (int i = 0; i < cycles; i++) {
			result.add(findExecutor.submit(new Finder(dataToFind)));
		}
		for (Future<Long> future : result) {
			try {
				time += future.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		findExecutor.shutdown();
		findingTime = time / cycles;
		return findingTime;
	}

	public long findRemovingTime(T dataToRemove) {
		ExecutorService removeExecutor = Executors.newFixedThreadPool(cycles);
		List<Future<Long>> result = new ArrayList<>();
		long time = 0;
		for (int i = 0; i < cycles; i++) {
			result.add(removeExecutor.submit(new Remover(dataToRemove)));
		}
		for (Future<Long> future : result) {
			try {
				time += future.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		removeExecutor.shutdown();
		removingTime = time / cycles;
		return removingTime;
	}

	public String toString() {
		Formatter formatter = new Formatter();
		formatter.format("\t|%-30s|%12s ns|%12s ns|%12s ns|", collectionName, addingTime, findingTime, removingTime);
		String result = formatter.toString();
		formatter.close();
		return result;
	}
}
