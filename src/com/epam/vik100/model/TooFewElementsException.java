package com.epam.vik100.model;

public class TooFewElementsException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public TooFewElementsException() {
		super("Too few elements to perform operation");
	}
}
