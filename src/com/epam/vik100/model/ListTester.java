package com.epam.vik100.model;

import java.util.Formatter;
import java.util.List;

public class ListTester<T> {
	private List<T> list;
	private String collectionName;
	private long addingTimeBegin;
	private long addingTimeCentre;
	private long addingTimeEnd;
	private long removingTimeBegin;
	private long removingTimeCentre;
	private long removingTimeEnd;
	private int amount;
	private int cycles;

	public int getAmount() {
		return amount;
	}

	public ListTester(List<T> list, int cycles, int amount) {
		this.list = list;
		collectionName = list.getClass().getSimpleName();
		this.amount = amount;
		this.cycles = cycles;
	}

	public List<T> getList() {
		return list;
	}

	protected long getAddingTime(int pos, T element) {
		long start = System.nanoTime();
		list.add(pos, element);
		return System.nanoTime() - start;
	}

	protected long getRemovingTime(int pos) {
		long start = System.nanoTime();
		list.remove(pos);
		return System.nanoTime() - start;
	}

	public void fillList(T element) {
		list.clear();
		int iter = amount;
		while (iter > 0) {
			list.add(element);
			iter--;
		}
	}

	public long findAddingTimeBegin(T element) {
		long result = 0;
		int iter = cycles;
		while (iter > 0) {
			result += getAddingTime(0, element);
			iter--;
		}
		addingTimeBegin = result / cycles;
		return addingTimeBegin;
	}

	public long findAddingTimeCenter(T element) {
		long result = 0;
		int iter = cycles;
		while (iter > 0) {
			result += getAddingTime(list.size() / 2, element);
			iter--;
		}
		addingTimeCentre = result / cycles;
		return addingTimeCentre;
	}

	public long findAddingTimeEnd(T element) {
		long result = 0;
		int iter = cycles;
		while (iter > 0) {
			result += getAddingTime(list.size(), element);
			iter--;
		}
		addingTimeEnd = result / cycles;
		return addingTimeEnd;
	}

	public long findRemovingTimeBegin(T anyObjOfClass) {
		fillList(anyObjOfClass);
		long result = 0;
		int iter = cycles;
		while (iter > 0) {
			result += getRemovingTime(0);
			iter--;
		}
		removingTimeBegin = result / cycles;
		return removingTimeBegin;
	}

	public long findRemovingTimeCenter(T anyObjOfClass) {
		fillList(anyObjOfClass);
		long result = 0;
		int iter = cycles;
		while (iter > 0) {
			result += getRemovingTime(list.size() / 2);
			iter--;
		}
		removingTimeCentre = result / cycles;
		return removingTimeCentre;
	}

	public long findRemovingTimeEnd(T anyObjOfClass) {
		fillList(anyObjOfClass);
		long result = 0;
		int iter = cycles;
		while (iter > 0) {
			result += getRemovingTime(list.size() - 1);
			iter--;
		}
		removingTimeEnd = result / cycles;
		return removingTimeEnd;
	}

	public String getCollectionName() {
		return collectionName;
	}

	private String convinientTime(long time) {
		return time + " ns";
	}

	public String toString() {
		Formatter formatter = new Formatter();
		formatter.format("\t|%-20s|%15s|%15s|%15s|%15s|%15s|%15s|", collectionName, convinientTime(addingTimeBegin),
				convinientTime(addingTimeCentre), convinientTime(addingTimeEnd), convinientTime(removingTimeBegin),
				convinientTime(removingTimeCentre), convinientTime(removingTimeEnd));
		String result = formatter.toString();
		formatter.close();
		return result;
	}
}
