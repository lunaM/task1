package com.epam.vik100.model;

import java.util.Formatter;
import java.util.Set;

public class SetTester<T> implements TestableSet<T> {
	private Set<T> set;
	private String collectionName;
	private long addingTime;
	private long removingTime;
	private long findElementTime;
	private int cycles;

	public SetTester(Set<T> set, int cycles) {
		this.set = set;
		collectionName = set.getClass().getSimpleName();
		this.cycles = cycles;
	}

	public long findAddingTime(T element) {
		long start = System.nanoTime();
		int iter = cycles;
		while (iter > 0) {
			set.add(element);
			iter--;
		}
		addingTime = (System.nanoTime() - start) / cycles;
		return addingTime;
	}

	@Override
	public void fillWithElements(T element, int amount, Generator<T> generator) {
		while (amount > 0) {
			set.add(generator.next());
			amount--;
		}
	}

	public long findRemovingTime(T element) throws TooFewElementsException {
		if (set.size() < cycles) {
			throw new TooFewElementsException();
		}
		long start = System.nanoTime();
		int iter = cycles;
		while (iter > 0) {
			set.remove(element);
			iter--;
		}
		removingTime = (System.nanoTime() - start) / cycles;
		return removingTime;
	}

	public long findFindingElementTime(T element) {
		set.add(element);
		int iter = cycles;
		long start = System.nanoTime();
		while (iter > 0) {
			set.contains(element);
			iter--;
		}
		findElementTime = (System.nanoTime() - start) / cycles;
		return findElementTime;
	}

	private String convinientTime(long time) {
		return time + " ns";
	}

	public String toString() {
		Formatter formatter = new Formatter();
		formatter.format("\t|%-30s|%10s|%10s|%10s|", collectionName, convinientTime(addingTime),
				convinientTime(removingTime), convinientTime(findElementTime));
		String result = formatter.toString();
		formatter.close();
		return result;
	}

	public String getCollectionName() {
		return collectionName;
	}

}
