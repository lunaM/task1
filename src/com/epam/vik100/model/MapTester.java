package com.epam.vik100.model;

import java.util.Formatter;
import java.util.Map;
import java.util.Random;

public class MapTester<S> {
	private Map<Integer, S> map;
	private String collectionName;
	private long puttingTime;
	private long gettingTime;
	private long removingTime;
	private long findElementByValueTime;
	private int cycles;

	public MapTester(Map<Integer, S> map, int cycles) {
		this.map = map;
		collectionName = map.getClass().getSimpleName();
		this.cycles = cycles;
	}

	public Map<Integer, S> getMap() {
		return map;
	}

	public void fillMap(S value, int amount) {
		map.clear();
		Random random = new Random(47);
		while (amount > 0) {
			map.put(random.nextInt(), value);
			amount--;
		}
	}

	public long findFindingByValueTime(S value) {
		int iter = cycles;
		long start = System.nanoTime();
		while (iter > 0) {
			map.containsValue(value);
			iter--;
		}
		findElementByValueTime = (System.nanoTime() - start) / cycles;
		return findElementByValueTime;
	}

	public long findPuttingTime(S value) {
		int iter = cycles;
		Random random = new Random(47);
		long start = System.nanoTime();
		while (iter > 0) {
			map.put(random.nextInt(), value);
			iter--;
		}
		puttingTime = (System.nanoTime() - start) / cycles;
		return puttingTime;
	}

	public long findGettingTime() {
		int iter = cycles;
		Random random = new Random(47);
		long start = System.nanoTime();
		while (iter > 0) {
			map.get(random.nextInt());
			iter--;
		}
		gettingTime = (System.nanoTime() - start) / cycles;
		return gettingTime;
	}

	public long findRemovingTime() throws TooFewElementsException {
		int iter = cycles;
		if (map.size() < cycles) {
			throw new TooFewElementsException();
		}
		long start = System.nanoTime();
		while (iter > 0) {
			map.remove(iter);
			iter--;
		}
		removingTime = (System.nanoTime() - start) / cycles;
		return removingTime;
	}

	public String getCollectionName() {
		return collectionName;
	}

	private String convinientTime(long time) {
		return time + " ns";
	}

	public String toString() {
		Formatter formatter = new Formatter();
		formatter.format("\t|%-20s|%15s|%15s|%15s|%15s|", collectionName, convinientTime(puttingTime),
				convinientTime(gettingTime), convinientTime(removingTime), convinientTime(findElementByValueTime));
		String result = formatter.toString();
		formatter.close();
		return result;
	}

}
